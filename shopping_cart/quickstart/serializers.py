from shopping_cart.quickstart.models import Items
from rest_framework import serializers

class ItemsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Items
        fields = ['id', 'name', 'price', 'quantity', ]