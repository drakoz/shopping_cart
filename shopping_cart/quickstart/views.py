from django.shortcuts import render
from rest_framework import viewsets, generics
from shopping_cart.quickstart.serializers import ItemsSerializer
from shopping_cart.quickstart.models import Items

class ItemsViewSet(viewsets.ModelViewSet):
    """
    API untuk items
    """
    queryset = Items.objects.all()
    serializer_class = ItemsSerializer